import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import classes from "./Aside.module.scss";

interface AsideType {
    title: string;
    link: string;
    className?: string;
}

const Aside: React.FC<any> = (props) => {
    const router = useRouter();

    const asideItems: AsideType[] = [
        { title: "خانه", link: "/" },
        { title: "درباره ما", link: "/about" },
    ];

    return (
        <aside className={props.className}>
            <ul>
                {asideItems.map((item, key) => (
                    <li key={key} className={item.link === router.pathname ? classes.active : ""}>
                        <Link href={item.link}>{item.title}</Link>
                    </li>
                ))}
            </ul>
        </aside>
    );
};

export default Aside;

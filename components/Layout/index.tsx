import React, { FC } from "react";
import Aside from "./../Aside/index";
import classes from "./Layout.module.scss";

const Layout: FC<any> = (props) => {
    return (
        <div className={classes.container}>
            <Aside className={classes.aside} />
            <main className={classes.main}>{props.children}</main>
        </div>
    );
};

export default Layout;

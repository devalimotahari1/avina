import React from "react";
import classes from "./Input.module.scss";

const Input: React.FC<any> = (props) => {
    return (
        <>
            <input className={`${classes.main} ${props.className}`} {...props} />
        </>
    );
};

export default Input;

import React from "react";
import classes from "./Button.module.scss";

const Button: React.FC<any> = (props) => {
    return (
        <>
            <button className={`${classes.main} ${props.className}`} {...props}>
                {props.children}
            </button>
        </>
    );
};

export default Button;

import React, { useState } from "react";
import { toast } from "react-toastify";
import { contactType } from "../../pages";
import Button from "../Button";
import Input from "../Input";

interface IProps {
    onSubmit: (formObj: contactType | undefined) => void;
}

const AddContact: React.FC<IProps> = (props) => {
    const [form, setForm] = useState<contactType>();

    const handleChange = (e: any) => {
        const {
            target: { name, value },
        } = e;
        setForm((p) => {
            const obj: any = { ...p };
            obj[name] = value;
            return obj;
        });
    };

    return (
        <>
            <form>
                <Input
                    required
                    placeholder="نام"
                    onChange={handleChange}
                    type={"text"}
                    name="firstName"
                />
                <Input
                    required
                    placeholder="نام خانوادگی"
                    onChange={handleChange}
                    type={"text"}
                    name="lastName"
                />
                <Input
                    required
                    placeholder="شماره تلفن"
                    onChange={handleChange}
                    type={"text"}
                    name="phoneNumber"
                    minLength="11"
                    maxLength="11"
                />
                <Input
                    required
                    placeholder="ایمیل"
                    onChange={handleChange}
                    type={"email"}
                    name="email"
                />

                <Button
                    style={{ marginTop: "10px" }}
                    onClick={() => {
                        if (
                            !form?.firstName ||
                            !form?.lastName ||
                            !form?.email ||
                            !form?.phoneNumber
                        )
                            return toast("لطفا تمامی موارد را کامل کنید.", { type: "warning" });
                        return props.onSubmit(form);
                    }}>
                    ذخیره
                </Button>
            </form>
        </>
    );
};

export default AddContact;

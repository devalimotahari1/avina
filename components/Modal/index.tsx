import React from "react";
import classes from "./Modal.module.scss";

interface IProps {
    isOpen: boolean;
    setIsOpen: Function;
    className?: string;
}

const Modal: React.FC<IProps> = (props) => {
    if (props.isOpen) {
        return (
            <>
                <div className={`${classes.shadow}`} onClick={() => props.setIsOpen(false)} />
                <div className={`${classes.container} ${props.className}`}>
                    <div className={classes.main}>
                        <span className={classes.close} onClick={() => props.setIsOpen(false)}>
                            close
                        </span>
                        <div className={classes.content}>{props.children}</div>
                    </div>
                </div>
            </>
        );
    } else return null;
};

export default Modal;

import React from "react";
import { contactType } from "../../pages";

interface IProps {
    contacts: contactType[];
}

const defaultProps: IProps = {
    contacts: [],
};

const ContactList: React.FC<IProps> = (props) => {
    if (props.contacts.length <= 0) return <>مخاطبی وجود ندارد!</>;
    return (
        <>
            <table style={{ width: "100%", margin: "20px 0" }}>
                <thead>
                    <tr>
                        <th>نام</th>
                        <th>نام خانوادگی</th>
                        <th>شماره موبایل</th>
                        <th>ایمیل</th>
                    </tr>
                </thead>
                <tbody>
                    {props.contacts.map((contact, key) => (
                        <tr key={key}>
                            <td>{contact.firstName}</td>
                            <td>{contact.lastName}</td>
                            <td>{contact.phoneNumber}</td>
                            <td>{contact.email}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
};

ContactList.defaultProps = defaultProps;

export default ContactList;

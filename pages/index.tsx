import type { NextPage } from "next";
import Head from "next/head";
import { useState } from "react";
import { toast } from "react-toastify";
import AddContact from "../components/AddContact";
import Button from "../components/Button";
import ContactList from "../components/ContactList";
import Modal from "../components/Modal";
import classes from "../styles/Home.module.scss";

export interface contactType {
    firstName: string;
    lastName: string;
    phoneNumber: number;
    email: string;
}

const Home: NextPage = () => {
    const [contacts, setContacts] = useState<contactType[]>([]);
    const [modalOpen, setModalOpen] = useState<boolean>(false);

    const addContact = (contact?: contactType) => {
        if (contact) {
            if (contacts.some((c) => c.phoneNumber === contact.phoneNumber)) {
                return toast("شماره موبایل مخاطب تکراری است.", { type: "error" });
            }
            setContacts((p) => {
                const arr = [...p];
                arr.push(contact);
                return arr;
            });
            setModalOpen(false);
        }
    };

    return (
        <>
            <Head>
                <title>Avina Test App | Dashboard</title>
                <meta name="description" content="Avina Test App" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className={classes.container}>
                <Button onClick={() => setModalOpen(true)}>+ New contact</Button>
            </div>
            <Modal isOpen={modalOpen} setIsOpen={setModalOpen}>
                <AddContact onSubmit={addContact} />
            </Modal>
            <ContactList contacts={contacts} />
        </>
    );
};

export default Home;

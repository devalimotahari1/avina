import React from "react";
import Head from "next/head";

const About = () => {
    return (
        <>
            <Head>
                <title>Avina Test App | About</title>
            </Head>
            <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi unde fugiat id
                laudantium fuga, natus quae commodi quas necessitatibus alias deserunt, aliquam
                dolorem iusto atque. Explicabo repellat placeat nesciunt nihil.
            </p>
        </>
    );
};

export default About;
